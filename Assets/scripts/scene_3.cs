﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.AccessControl;
using UnityEngine.UI;
using System;

public class scene_3 : MonoBehaviour {

	public GameObject Content; 
	public GameObject item;

	// Use this for initialization
	void Start () {
		item.SetActive (false);
	}

	public void AddLast(){
		GameObject tmp = Instantiate (item, Content.transform);
		tmp.SetActive (true);
		tmp.transform.FindChild ("index").gameObject.GetComponent<Text> ().text = (Content.transform.childCount-1).ToString();
	}

	public void removeLast(){
		if(Content.transform.childCount>1)
			
			Destroy (Content.transform.GetChild(Content.transform.childCount-1).gameObject);
	}
		
	public void removebyItem(GameObject item){
		for(int j = 0 ; j < Content.transform.childCount ;j++){
			if(item == Content.transform.GetChild (j).gameObject)
			Destroy (Content.transform.GetChild (j).gameObject);

		}
	}

}

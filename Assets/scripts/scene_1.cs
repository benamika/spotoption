﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class scene_1 : MonoBehaviour {

	public GameObject cross, moevArea;

	public Vector2 bottomLeft, topRight;

    float selfWitdh, selfHeight ;

	Vector3 touchPosWorld, nextPos;

	TouchPhase touchPhase = TouchPhase.Ended;

	void Start () {
        selfWitdh = cross.GetComponent<Collider2D>().bounds.size.x;
        selfHeight = cross.GetComponent<Collider2D>().bounds.size.y;

        Vector2 center = moevArea.GetComponent<RectTransform>().rect.center;
        
        bottomLeft = topRight = Camera.main.ScreenToWorldPoint(center);
        topRight = topRight * -1;
        bottomLeft = new Vector2(bottomLeft.x + (selfWitdh / 2) , bottomLeft.x + (selfHeight / 2));
        topRight = new Vector2(topRight.x - (selfWitdh / 2), topRight.x + (selfHeight / 2));

    }

    void Update () {
       
        if (Input.GetMouseButtonDown(0))
        {
			Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            
			if (hit.collider != null)
			{
                moveCross (Camera.main.WorldToScreenPoint(worldPoint));
			}
		}

		if (Input.touchCount > 0 && Input.GetTouch(0).phase == touchPhase) {
			touchPosWorld = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

			Vector2 touchPosWorld2D = new Vector2(touchPosWorld.x, touchPosWorld.y);

			RaycastHit2D hitInformation = Physics2D.Raycast(touchPosWorld2D, Camera.main.transform.forward);

			if (hitInformation.collider != null) {
                moveCross(Camera.main.WorldToScreenPoint( touchPosWorld2D));
            }
		}

	}

	void moveCross(Vector2 worldP)
	{
       nextPos =new Vector3(UnityEngine.Random.Range(bottomLeft.x, topRight.x),
            UnityEngine.Random.Range(bottomLeft.y, topRight.y),
            0);
        cross.GetComponent<moveGameObject>().endMarker = nextPos;
        cross.GetComponent<moveGameObject>().canMove = true;
        cross.GetComponent<moveGameObject>().startMoving();
    }

	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scene_2 : MonoBehaviour {


    public GameObject box, moveArea;

    public Vector2 bottomLeft, topRight;

    float selfWitdh, selfHeight;

    Vector3 touchPosWorld, nextPos;

    TouchPhase touchPhase = TouchPhase.Ended;

    void Start()
    {
        box = GameObject.Find("Cube");
        moveArea = GameObject.Find("moveArea"); 
        selfWitdh = box.GetComponent<Collider>().bounds.size.x;
        selfHeight = box.GetComponent<Collider>().bounds.size.y;

        Vector2 center = moveArea.GetComponent<RectTransform>().rect.center;

        bottomLeft = topRight = Camera.main.ScreenToWorldPoint(center);
        topRight = topRight * -1;
        bottomLeft = new Vector2(bottomLeft.x + (selfWitdh / 2), bottomLeft.x + (selfHeight / 2));
        topRight = new Vector2(topRight.x - (selfWitdh / 2), topRight.x + (selfHeight / 2));

    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            
            {
                moveCross(Camera.main.WorldToScreenPoint(ray.direction));
            }
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == touchPhase)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                moveCross(Camera.main.WorldToScreenPoint(ray.direction));
            }
        }

    }

    void moveCross(Vector2 worldP)
    {
        nextPos = new Vector3(UnityEngine.Random.Range(bottomLeft.x, topRight.x),
             UnityEngine.Random.Range(bottomLeft.y, topRight.y),
             0);

        box.GetComponent<moveGameObject>().endMarker = nextPos;
        box.GetComponent<moveGameObject>().canMove = true;
        box.GetComponent<moveGameObject>().startMoving();
        box.transform.Rotate(new Vector3(15f, 15f));
    }

}

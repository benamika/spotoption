﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveGameObject : MonoBehaviour {
    public Vector3 endMarker;
    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;
    public bool canMove = false;
    public void startMoving()
    {
        startTime = Time.time;
        journeyLength = Vector3.Distance(transform.position, endMarker);
    }
    void Update()
    {
        if (canMove)
        {
            if (transform.position.Equals(endMarker))
            canMove = false;
    
            float distCovered = (Time.time - startTime) * speed;
            float fracJourney = distCovered / journeyLength;
            transform.position = Vector3.Lerp(transform.position, endMarker, fracJourney);
        }
        
    }
}

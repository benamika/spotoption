﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Collections;
using UnityEngine.SceneManagement;

public class menuCtrl : MonoBehaviour {


	public List<Transform> menuWInds;
	public List<Transform> scenes;

	Stack <int> scenesStack= new Stack<int>();

	void Start () {
		openWindByNum (0);
	}
	
	void Update () {
		if (Input.GetKey ("escape"))
			back ();
	}

	public void openWindByNum(int ind){
		for(int i = 0 ; i < menuWInds.Count ; i++){
			if (menuWInds [i].gameObject.activeSelf) {
				scenesStack.Push (i);
			}

		}
		closeAll ();
		menuWInds [ind].gameObject.SetActive (true);
		scenes [ind].gameObject.SetActive (true);
	}

	void closeAll(){
	
		foreach(Transform wind in menuWInds){
			wind.gameObject.SetActive(false);
		}
		foreach(Transform scene in scenes){
			scene.gameObject.SetActive(false);
		}
	}

	public void back(){
		closeAll ();
		if (scenesStack.Count > 0)
			openWindByNum (scenesStack.Pop ());
		else {
			Application.Quit();
		}
	}

}
